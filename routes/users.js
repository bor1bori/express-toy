var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const ObjectId = require('mongoose').Types.ObjectId;
mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});

const UserModel = mongoose.model('User', {id: String, password: String});

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/register', (req, res) => {
  if (!req.body.id || !req.body.password) {
    res.status(400).json({description: 'id or password is not in body', success: false});
    return;
  }
  UserModel.create({id: req.body.id, password: req.body.password})
  .then((user) => {
    res.status(200).json({success: true});
  })
  .catch((err) => {
    res.status(500).json({descrtipion: err, success: false});
  });

})

router.post('/login', (req, res) => {
  if (!req.body.id || !req.body.password) {
    res.status(400).json({description: 'id or password is not in body', success: false})
  }
  UserModel.findOne({id: req.body.id})
  .then((user) => {
    if (!user) {
      res.status(404).json({description: `존재하지 않는 id입니다. (got ${req.body.id})`, success: false});
      return;
    }
    if (user.password === req.body.password) {
      res.status(200).json({success: true, token: user._id});
    } else {
      res.status(403).json({description: '비밀번호가 잘못되었습니다.', success: false});
    }
  })
  .catch((err) => {
    res.status(500).json({description: err, success: false});
  });
});

router.get('/who-am-i', (req, res) => {
  if (!ObjectId.isValid(req.query.id)) {
    res.status(400).json({description: `wrong token (${req.query.id}), get to user/who-am-i?id=\${token}`});
  }
  UserModel.findById(req.query.id)
  .then((user) => {
    if (user) {
      res.status(200).json({answer: `you are ${user.id}`, success: true});
    } else {
      res.status(404).json({description: 'token is worng', success: false});
    }
  })
  .catch((err) => {
    res.status(500).json({description: err, success: false});
  });
});

module.exports = router;
